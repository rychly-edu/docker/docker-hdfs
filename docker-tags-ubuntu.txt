# tags (before "\t") and build arguments (after "\t", separated by " " which cannot be utilised otherwise) of the Docker image variants to build
# for versions, see https://hadoop.apache.org/releases.html
#2.6.5	HADOOP_VERSION=2.6.5
#2.7.7	HADOOP_VERSION=2.7.7
#2.8.5	HADOOP_VERSION=2.8.5
2.9.2	HADOOP_VERSION=2.9.2
2.10.0	HADOOP_VERSION=2.10.0
#3.0.3	HADOOP_VERSION=3.0.3
3.1.3	HADOOP_VERSION=3.1.3
3.2.1	HADOOP_VERSION=3.2.1
