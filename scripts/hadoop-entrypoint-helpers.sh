#!/usr/bin/env bash

warn() {
	local varname="${1}"
	shift
	echo "Variable '${varname}' has to/should be set correctly!" >&2
	echo $@
}

die() {
	warn $@
	exit 1
}

make_hadoop_dirs() {
	local singledir dirlist="${1}"
	for singledir in ${dirlist//,/ }; do
		mkdir -vp "${singledir}"
		chmod -v 700 "${singledir}"
		chown -R hadoop:hadoop "${singledir}"
	done
}

set_nn_addresses() {
	local dfs_namenode dfs_namenodes="${1}"
	for dfs_namenode in ${dfs_namenodes//,/ }; do
		case "${dfs_namenode}" in
			http://*)
				if [ -z "${PROP_HDFS_dfs_namenode_http___address}" ]; then
					export PROP_HDFS_dfs_namenode_http___address="${dfs_namenode#http://}"
				elif [ -z "${PROP_HDFS_dfs_namenode_secondary_http___address}" ]; then
					export PROP_HDFS_dfs_namenode_secondary_http___address="${dfs_namenode#http://}"
				else
					die DFS_NAMENODES "Cannot add namenode URL '${dfs_namenode}' as both primary and secondary namenodes are already defined. Try HDFS HA for more namenodes."
				fi
				;;
			https://*)
				if [ -z "${PROP_HDFS_dfs_namenode_https___address}" ]; then
					export PROP_HDFS_dfs_namenode_https___address="${dfs_namenode#https://}"
				elif [ -z "${PROP_HDFS_dfs_namenode_secondary_https___address}" ]; then
					export PROP_HDFS_dfs_namenode_secondary_https___address="${dfs_namenode#https://}"
				else
					die DFS_NAMENODES "Cannot add namenode URL '${dfs_namenode}' as both primary and secondary namenodes are already defined. Try HDFS HA for more namenodes."
				fi
				;;
			rpc://*)	export PROP_HDFS_dfs_namenode_rpc___address="${dfs_namenode#rpc://}" ;;
			srvrpc://*)	export PROP_HDFS_dfs_namenode_servicerpc___address="${dfs_namenode#srvrpc://}" ;;
			llrpc://*)	export PROP_HDFS_dfs_namenode_lifeline_rpc___address="${dfs_namenode#llrpc://}" ;;
			*)	die DFS_NAMENODES "Unknown protocol in '${dfs_namenode}' (must be one of http://, https://, rpc://, srvrpc://, or llrpc://)."
		esac
	done
}

set_dn_addresses() {
	local dfs_datanode dfs_datanodes="${1}"
	for dfs_datanode in ${dfs_datanodes//,/ }; do
		case "${dfs_datanode}" in
			data://*)	export PROP_HDFS_dfs_datanode_address="${dfs_datanode#data://}" ;;
			http://*)	export PROP_HDFS_dfs_datanode_http_address="${dfs_datanode#http://}" ;;
			https://*)	export PROP_HDFS_dfs_datanode_https_address="${dfs_datanode#https://}" ;;
			ipc://*)	export PROP_HDFS_dfs_datanode_ipc_address="${dfs_datanode#ipc://}" ;;
			*)	die DFS_DATANODES "Unknown protocol in '${dfs_datanode}' (must be one of data://, http://, https://, or ipc://)."
		esac
	done
}

# Functions to set the HDFS properties, see
# https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/hdfs-default.xml

set_dfs_default() {
	[ -n "${DFS_DEFAULT}" ] && export PROP_CORE_fs_defaultFS="${DFS_DEFAULT}" \
	|| die DFS_DEFAULT "The default path prefix used by the Hadoop FS client when none is given.\
 This is the URI: protocol specifier, e.g., 'hdfs://'; hostname and port of the namenode for the cluster (its RPC port for the 'dhfs://' protocol)."
}

set_data_dirs() {
	if [ -n "${DATA_DIRS}" ]; then
		make_hadoop_dirs "${DATA_DIRS}"
		export PROP_HDFS_dfs_datanode_data_dir="file://${DATA_DIRS//,/,file://}"
	else
		die DATA_DIRS "Determines where on the local filesystem an DFS data node should store its blocks.\
 If this is a comma-delimited list of directories, then data will be stored in all named directories, typically on different devices."
	fi
}

set_name_dirs() {
	if [ -n "${NAME_DIRS}" ]; then
		make_hadoop_dirs "${NAME_DIRS}"
		export PROP_HDFS_dfs_namenode_name_dir="file://${NAME_DIRS//,/,file://}"
	else
		die NAME_DIRS "Determines where on the local filesystem the DFS name node should store the name table(fsimage).\
 If this is a comma-delimited list of directories then the name table is replicated in all of the directories, for redundancy."
	fi
}

set_dfs_namenodes() {
	if [ -n "${DFS_NAMENODES}" ]; then
		set_nn_addresses "${DFS_NAMENODES}"
	else
		warn DFS_NAMENODES "The protocols, addresses and the base ports where the DFS namenode HTTP, HTTPS, RPC, Service RPC, and LifeLine RPC will listen on.\
 It can be a comma-delimited list of such URLs."
	fi
}

set_dfs_datanodes() {
	if [ -n "${DFS_DATANODES}" ]; then
		set_dn_addresses "${DFS_DATANODES}"
	else
		warn DFS_DATANODES "The protocols, addresses and the base ports where the DFS datanode DATA, HTTP, HTTPS, and IPC will listen on.\
 It can be a comma-delimited list of such URLs."
	fi
}

add_users() {
	# add some users (no passwords and no homes) to be able to use them in HDFS (e.g., spark user for hdfs://namenode:8020/user/spark/applicationHistory)
	local add_user
	if adduser --help 2>&1 | grep -qF BusyBox; then
		ADDUSER='adduser -D -H -g HDFS_user -G users'
	else
		ADDUSER='adduser --disabled-login --no-create-home --gecos HDFS_user --ingroup users'
	fi
	for add_user in ${ADD_USERS//,/ }; do
		${ADDUSER} "${add_user}"
	done
}
